from rest_framework import serializers
from users.models import NewUser


class CustomUserSerializer(serializers.ModelSerializer):
    """
    Currently unused in preference of the below.
    """
    email = serializers.EmailField(required=True)
    user_name = serializers.CharField(required=True)
    password = serializers.CharField(min_length=8, write_only=True)
    #https://stackoverflow.com/questions/41332528/how-to-hash-django-user-password-in-django-rest-framework
    #https://forum.djangoproject.com/t/correct-way-to-let-user-register-sign-up-with-drf/1370/5

    class Meta:
        model = NewUser
        fields = ('email', 'user_name', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        # as long as the fields are the same, we can just use this
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


# class UserSerializer(serializers.ModelSerializer):
#     name = serializers.SerializerMethodField(read_only=True)
#     _id = serializers.SerializerMethodField(read_only=True)
#     isAdmin = serializers.SerializerMethodField(read_only=True)

#     class Meta:
#         model = User
#         fields = ['id', '_id', 'username', 'email', 'name', 'isAdmin']

#     def get__id(self, obj):
#         return obj.id

#     def get_isAdmin(self, obj):
#         return obj.is_staff

#     def get_name(self, obj):
#         name = obj.first_name
#         if name == '':
#             name = obj.email

#         return name


# from rest_framework import viewsets
# from . import models
# from . import serializers

# from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
# from rest_framework_simplejwt.viewset import TokenObtainPairView

# class ProductViewset(viewsets.ModelViewSet):
#     queryset = models.Product.objects.all()
#     serializer_class = serializers.ProductSerializer


# class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
#     @classmethod
#     def get_token(cls, user):
#         token = super().get_token(user)

#         # Add custom claims
#         token['name'] = user.name
#         # ...

#         return token


# class MyTokenObtainPairViewset(MyTokenObtainPairViewset):
#     serializer_class = MyTokenObtainPairSerializer

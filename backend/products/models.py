from django.db import models
#from users.models import Users
from django.utils import timezone
from django.conf import settings


# Create your models here.


class Product(models.Model):

    product_id = models.AutoField(primary_key=True, editable=False)
    product_name = models.CharField(max_length=200, null=True, blank=True)
    product_catagory = models.CharField(max_length=100, null=True, blank=True)
    product_description = models.TextField(null=True, blank=True)
    product_price = models.DecimalField(
        max_digits=7, decimal_places=2, null=True, blank=True)
    product_countInStock = models.IntegerField(null=True, blank=True, default=0)

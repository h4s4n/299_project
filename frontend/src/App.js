import { Container } from "react-bootstrap";
import Header from "./components/header";
import Footer from "./components/footer";
import Register from "./components/register";
// import { register } from "../../../Demo/frontend/src/actions/userActions";


function App() {
  return (
    <div>
      <Header />
      <main className="py-5">
        <Container>
          <Route path="/register" component={Register} />
          <h1>Welcome to Book Club</h1>
        </Container>
      </main>
      <Footer />
    </div>
  );
}

export default App;
